Проект OSCAR является ответвлением проекта [Apollo](https://github.com/ApolloAuto/apollo).

### Установка ПО

1. Установите [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

2. Склонируйте репозиторий проекта:

    ```
    git clone https://gitlab.com/starline/oscar.git
    ```

3. Задайте переменные окружения для использования *oscar_tools*:

    ```
    echo "source path-to-oscar-root/scripts/oscar/oscar_tools/setup.sh" >> ~/.bashrc
    source ~/.bashrc
    ```

    , где **path-to-oscar-root** - путь до репозитория на вашей системе.

    *oscar_tools* - набор утилит, позволяющий упростить навигацию по проекту и реализующий интерфейс к разрабатываему в рамках проекта ПО.

    [Документация по использованию oscar_tools](oscar_tools.md)


### Работа с проектом

1. Для запуска всех необходимых docker-контейнеров проекта выполните:

    ```
    oscar docker start
    ```

    В случае первого запуска после клонирования репозитория или в случае отсутствия требуемых docker-образов, с удаленного docker-хранилища Apollo автоматически будут подтянуты все требуемые образы и собраны все контейнеры.

    Для остановки docker-контейнеров Apollo можно выполнить:

    ```
    oscar docker stop
    ```

    Для последующих запусков по умолчанию будут использоваться остановленные контейнеры. Для полного перезапуска контейнеров можно воспользоваться следующей командой:

    ```
    oscar docker rerun_containers
    ```

    Для обновления docker-образов Apollo можно воспользоваться следующей командой:

    ```
    oscar docker pull_images
    ```

4. Для работы с Apollo войдите в рабочий docker-контейнер:

    ```
    oscar docker into
    ```

5. Для сборки всего проекта выполните:

    ```
    bash apollo.sh build
    ```

    Для сборки с поддержкой GPU выполните:

    ```
    bash apollo.sh build_gpu
    ```

6. Запустите Dreamview для начала работы с системой беспилотного вождения:

    ```
    bash scripts/bootstrap.sh
    ```

    Dreamview - веб-интерфейс, по умолчанию доступный на localhost:8888. Предоставляет средства визуализации и интерфейс для работы с модулями системы.


В случае успешного запуска Dreamview работу с проектом можно продолжить используя [симулятор LGSVL](lgsvl_simulator.md):

[![](http://img.youtube.com/vi/9XCLKZ7Pb8s/0.jpg)](http://www.youtube.com/watch?v=9XCLKZ7Pb8s "")

[Устранение возможных проблем](possible_problems.md)


### Настройка и калибровка датчиков

Для калибровки камер, а так же определения и настройки фокусного растояния камер ознакомтесь с соответствующией инструкцией - [README](../../scripts/oscar/sensor_calibration/camera/README.md).
