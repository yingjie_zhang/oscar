cyber_modules {
  key: "Canbus"
  value: {
    dag_files: "/apollo/modules/canbus/dag/canbus.dag"
  }
}
cyber_modules {
  key: "Control"
  value: {
    dag_files: "/apollo/modules/control/dag/control.dag"
  }
}
cyber_modules {
  key: "Guardian"
  value: {
    dag_files: "/apollo/modules/guardian/dag/guardian.dag"
  }
}
cyber_modules {
  key: "Localization"
  value: {
    dag_files: "/apollo/modules/localization/dag/dag_streaming_rtk_localization.dag"
  }
}
modules {
  key: "RTK Recorder"
  value: {
    start_command: "nohup /apollo/scripts/rtk_recorder.sh start &"
    stop_command: "/apollo/scripts/rtk_recorder.sh stop"
    process_monitor_config {
      command_keywords: "record_play/rtk_recorder.py"
    }
    # RTK Recorder is not running in self-driving mode.
    required_for_safety: false
  }
}
modules {
  key: "RTK Player"
  value: {
    start_command: "nohup /apollo/scripts/rtk_player.sh start &"
    stop_command: "/apollo/scripts/rtk_player.sh stop"
    process_monitor_config {
      command_keywords: "record_play/rtk_player.py"
    }
  }
}
modules {
  key: "Traj_player"
  value: {
    start_command: "nohup oscar trajectory play --no-loop &"
    stop_command: "oscar trajectory stop"
    process_monitor_config {
      command_keywords: "oscar_tools/trajectories/play_trajectory.py --no-loop"
    }
  }
}
modules {
  key: "Traj_player_l"
  value: {
    start_command: "nohup oscar trajectory play -l &"
    stop_command: "oscar trajectory stop"
    process_monitor_config {
      command_keywords: "oscar_tools/trajectories/play_trajectory.py -l"
    }
  }
}
modules {
  key: "Data Recorder"
  value: {
    start_command: "/apollo/scripts/record_bag.py --start"
    stop_command: "/apollo/scripts/record_bag.py --stop"
    process_monitor_config {
      command_keywords: "cyber_recorder"
    }
  }
}
monitored_components {
  key: "Localization"
  value: {
    # Special LocalizationMonitor.
  }
}
cyber_modules {
  key: "Planning"
  value: {
    dag_files: "/apollo/modules/planning/dag/planning.dag"
  }
}
cyber_modules {
  key: "Prediction"
  value: {
    dag_files: "/apollo/modules/prediction/dag/prediction_navi.dag"
  }
}
cyber_modules {
  key: "Fake_Prediction"
  value: {
    dag_files: "/apollo/modules/tools/prediction/fake_prediction/fake_prediction.dag"
  }
}
cyber_modules {
  key: "Routing"
  value: {
    dag_files: "/apollo/modules/routing/dag/routing.dag"
  }
}
cyber_modules {
  key: "Relative Map"
  value: {
    dag_files: "/apollo/modules/map/relative_map/dag/relative_map.dag"
  }
}
monitored_components {
  key: "Data Recorder"
  value: {
    process {
      command_keywords: "cyber_recorder"
    }
    resource {
      disk_spaces {
        # For logs.
        path: "/apollo/data"
        insufficient_space_warning: 8
        insufficient_space_error: 2
      }
      disk_spaces {
        # For records.
        path: "/media/apollo/internal_nvme"
        insufficient_space_warning: 128
        insufficient_space_error: 32
      }
    }
  }
}
